{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Fermions3d.GNYPsiEps where

import           Blocks                                      (BlockFetchContext,
                                                              Coordinate (XT),
                                                              CrossingMat,
                                                              Delta (..),
                                                              Derivative (..),
                                                              TaylorCoeff (..),
                                                              Taylors,
                                                              unzipBlock)
import qualified Blocks.Blocks3d                             as B3d
import           Blocks.Blocks3d.Build                       (block3dBuildLink)
import qualified Bootstrap.Bounds.BootstrapSDP               as BSDP
import           Bootstrap.Bounds.BoundDirection             (BoundDirection,
                                                              boundDirSign)
import           Bootstrap.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                              HasRep (..),
                                                              OPECoefficient,
                                                              OPECoefficientExternal,
                                                              ThreePointStructure (..),
                                                              crossingMatrix,
                                                              crossingMatrixExternal,
                                                              derivsVec,
                                                              mapBlocksFreeVect,
                                                              mapOps,
                                                              opeCoeffGeneric_,
                                                              opeCoeffIdentical_,
                                                              runTagged2)
import           Bootstrap.Bounds.Spectrum                   (DeltaRange,
                                                              Spectrum,
                                                              listDeltas)
import           Bootstrap.Build                             (FetchConfig (..),
                                                              SomeBuildChain (..),
                                                              noDeps)
import           Bootstrap.Math.FreeVect                     (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect                     as FV
import           Bootstrap.Math.HalfInteger                  (HalfInteger)
import qualified Bootstrap.Math.HalfInteger                  as HI
import           Bootstrap.Math.Linear                       (toV)
import qualified Bootstrap.Math.Linear                       as L
import           Bootstrap.Math.VectorSpace                  (zero, (*^))
import           Bounds.Fermions3d.GNY                       (ChannelType (..),
                                                              so3)
import qualified Bounds.Fermions3d.GNY                       as GNY
import           Bounds.Scalars3d.ONRep                      (ON3PtStruct (..),
                                                              ON4PtStruct (..),
                                                              ONRep (..),
                                                              evalONBlock)
import           Control.Monad.IO.Class                      (liftIO)
import           Data.Aeson                                  (FromJSON, ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Data                                   (Typeable)
import           Data.Matrix.Static                          (Matrix)
import           Data.Reflection                             (Reifies, reflect)
import           Data.Tagged                                 (Tagged)
import           Data.Traversable                            (for)
import           Data.Vector                                 (Vector)
import           GHC.Generics                                (Generic)
import           GHC.TypeNats                                (KnownNat)
import           Hyperion                                    (Dict (..),
                                                              Static (..), cPtr)
import           Hyperion.Bootstrap.Bound                    (BuildInJob,
                                                              SDPFetchBuildConfig (..),
                                                              ToSDP (..),
                                                              blockDir)
import           Linear.V                                    (V)
import qualified SDPB

data ExternalOp s = Psi | Eps
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaPsi :: Rational
  , deltaEps :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (B3d.ConformalRep Rational, ONRep) where
  rep x = case x of
    Psi -> (B3d.ConformalRep dPsi (1/2), ONVector)
    Eps -> (B3d.ConformalRep dEps 0,     ONSinglet)
    where
      ExternalDims {deltaPsi = dPsi, deltaEps = dEps} = reflect x

data GNYPsiEps = GNYPsiEps
  { externalDims :: ExternalDims
  , nGroup       :: Rational
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound BoundDirection
  | FeasibilitySig Rational (Maybe (V 3 Rational))
  -- | ExternalOPEBound BoundDirection
  -- | ConservedCurrentOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- | A channel with crossing matrices of size jxj containing blocks of
-- type 'b'
data Channel j b where
  Scalar_ParityEven_Singlet        :: Delta                  -> Channel 2 B3d.Block3d
  SpinEven_ParityEven_Singlet      :: B3d.ConformalRep Delta -> Channel 3 B3d.Block3d
  SpinEven_ParityOdd_Singlet       :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityOdd_Singlet        :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  Scalar_ParityEven_TensorSym      :: Delta                  -> Channel 1 B3d.Block3d
  SpinEven_ParityEven_TensorSym    :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinEven_ParityOdd_TensorSym     :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityOdd_TensorSym      :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityEven_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinOdd_ParityOdd_TensorAntiSym  :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinEven_ParityOdd_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityEven_Vector                :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityOdd_Vector                 :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  IdentityChannel     :: Channel 1 B3d.IdentityBlock3d
  StressTensorChannel :: Channel 1 B3d.Block3d
  CurrentChannel      :: Channel 2 B3d.Block3d
  ExternalOpChannel   :: Channel 2 B3d.Block3d
  ExternalSigChannel  :: Delta -> Channel 3 B3d.Block3d

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: KnownNat j -- ^ Existentially quantify over j
    => DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel j B3d.Block3d -- ^ The associated Channel
    -> BulkConstraint


------------------ Crossing equations ---------------------



-- | Crossing equations of the GNY model with N /= 1
crossingEqsGNYPsiEps
  :: forall n s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (B3d.Q4Struct, ON4PtStruct n) b a
  -> V 22 (Taylors 'XT, FreeVect b a)
crossingEqsGNYPsiEps g =
  GNY.crossingEqsGNY (mapOps f2 g)
  where
    f2 :: GNY.ExternalOp s -> ExternalOp t
    f2 GNY.Sig = Eps
    f2 GNY.Psi = Psi

-- | Crossing equations of the GNY model with N=1 (or the N=1 SUSY
-- Ising model, without SUSY taken into account)
--crossingEqsN1Eps
--  :: forall s a b . (Ord b, Fractional a, Eq a)
--  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
--  -> V 12 (Taylors 'XT, FreeVect b a)
--crossingEqsN1 g =
--  FFFF.crossingEqsWithFlavorSign xEven (mapOps (const Psi) g) L.++
--  crossingEqsFFSS g L.++
--  crossingEqSSSS  g

derivsVecGNYPsiEps :: GNYPsiEps -> V 22 (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecGNYPsiEps f =
  fmap ($ B3d.nmax (blockParams f)) (derivsVec crossingEqsGNYPsiEps)

crossingMatGNYPsiEps
  :: forall j n s a. (Reifies n Rational, KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
  -> Tagged '(n,s) (CrossingMat j 22 B3d.Block3d a)
crossingMatGNYPsiEps channel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix channel ((crossingEqsGNYPsiEps @n @s))
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)


mat
  :: forall j b n s a . (Reifies s ExternalDims, Reifies n Rational, Fractional a, Floating a, Eq a)
  => Channel j b
  ->  Tagged '(n,s) (CrossingMat j 22 b a)

-- Singlet r1 r2
mat (Scalar_ParityEven_Singlet deltaScalar) = crossingMatGNYPsiEps $ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 0))
  , opeCoeffIdentical_ Eps iRep (so3 0 0)
  )
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSinglet)

mat (SpinEven_ParityEven_Singlet iConformalRep) = crossingMatGNYPsiEps $ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  , opeCoeffIdentical_ Eps iRep (so3 0 l)
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep

-- Singlet r3
mat (SpinEven_ParityOdd_Singlet iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Singlet r4
mat (SpinOdd_ParityOdd_Singlet iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Symmetric Tensor r1 r2
-- (Essentially the same as Singlet)
mat (Scalar_ParityEven_TensorSym deltaScalar) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep  ((so3 0 0))
  )
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSymTensor)

mat (SpinEven_ParityEven_TensorSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep

-- Symmetric Tensor  r3
mat (SpinEven_ParityOdd_TensorSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Symmetric Tensor  r4
mat (SpinOdd_ParityOdd_TensorSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Antisymmetric Tensor r1 r2 (exchange even and odd spin )
mat (SpinOdd_ParityEven_TensorAntiSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep ((so3 0 l))
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep

-- Antisymmetric Tensor r3
mat (SpinOdd_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Antisymmetric Tensor r4
mat (SpinEven_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNYPsiEps$ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)



-- psi * sig , psi * eps
-- Flipped signs
mat (ParityEven_Vector iConformalRep) = crossingMatGNYPsiEps$ toV
  (  opeCoeffGeneric_ Psi Eps iRep (so3 (1/2) (l+1/2)) ((-1)^(round (l-1/2) :: Int) *^ (so3 (1/2) (l-1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep

mat (ParityOdd_Vector iConformalRep) = crossingMatGNYPsiEps$ toV
  (  opeCoeffGeneric_ Psi Eps iRep (so3 (1/2) (l-1/2)) ((-1)^(round (l+1/2) :: Int) *^ (so3 (1/2) (l+1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep


-- Identity
mat IdentityChannel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix (toV identityOpe) (crossingEqsGNYPsiEps @n @s)
  where
    evalFlavorCoeff (b,f) =  evalONBlock f *^ vec (B3d.IdentityBlock3d b)
    identityOpe o1 o2
      | o1 == o2  = vec ((fst o1rep, ON3PtStruct (snd o1rep) (snd o1rep) ONSinglet))
      | otherwise = 0
      where o1rep = rep @(ExternalOp s) o1


-- StressTensor
mat StressTensorChannel = crossingMatGNYPsiEps(toV stressTensorOpe)
  where
    stressTensorRep = (B3d.ConformalRep (RelativeUnitarity 0) 2, ONSinglet)
    psiTStruct = - 3/(2*sqrt 2*pi) *^ so3 1 2 + (sqrt 3*dPsi)/(2*pi) *^ so3 0 2
    -- sigTStruct = - (sqrt 3*dSig)/(2*sqrt 2*pi) *^ so3 0 2
    epsTStruct = - (sqrt 3*dEps)/(2*sqrt 2*pi) *^ so3 0 2
    dPsi = fromRational (deltaPsi (reflect @s Psi))
    -- dSig = fromRational (deltaSig (reflect @s Sig))
    dEps = fromRational (deltaEps (reflect @s Eps))
    stressTensorOpe o1 o2 =
      FV.mapBasis (makeStructure (rep o1) (rep o2) stressTensorRep) $
      case (o1, o2) of
        (Psi, Psi) -> psiTStruct
        --(Sig, Sig) -> sigTStruct
        (Eps, Eps) -> epsTStruct
        _          -> 0


-- Conserved Current
mat CurrentChannel = mat (SpinOdd_ParityEven_TensorAntiSym (fst currentRep))
  where
    currentRep = (B3d.ConformalRep (RelativeUnitarity 0) 1, ONAntiSymTensor)
    -- Not using the Ward identity for now
    -- psiTStruct = - 1/(sqrt 2*pi) *^ so3 0 1
    -- fixedCurrentOPE o1 o2
    --   | (o1 == o2 && o1 == Psi)  = FV.mapBasis (makeStructure (rep o1) (rep o1) (currentRep)) psiTStruct
    --   | otherwise = zero


mat ExternalOpChannel =
  pure . mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsGNYPsiEps @n) [Psi,Eps]
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)
    opeCoefficients :: V 2 (OPECoefficientExternal (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
    opeCoefficients = toV $ ((\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                             case (o1, o2, o3) of
                             (Psi,Psi,Eps) -> so3 0 0  -- -- Flipped sign, the signs of other psi*psi*O channels are not flipped
                             (Psi,Eps,Psi) -> so3 (1/2) 1
                             (Eps,Psi,Psi) -> so3 (1/2) 0 -- Flipped sign
                             _             -> 0)
                            , (\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                            case (o1, o2, o3) of
                            (Eps,Eps,Eps) -> so3 0 0
                            _             -> 0)
                             )

mat (ExternalSigChannel deltaSig) = do
  sig <- mat (SpinEven_ParityOdd_Singlet $ B3d.ConformalRep deltaSig 0)
  ext <- mat ExternalOpChannel
  return $ L.directSum sig ext

getExtMat
  :: (BlockFetchContext B3d.Block3d a f, Applicative f, RealFloat a)
  => GNYPsiEps
  -> f (Matrix 2 2 (Vector a))
getExtMat g =
  BSDP.getIsolatedMat (blockParams g) (derivsVecGNYPsiEps g) (runTagged2 (nGroup g, externalDims g) (mat ExternalOpChannel))

getSigExtMat
  :: (BlockFetchContext B3d.Block3d a f, Applicative f, RealFloat a)
  => GNYPsiEps
  -> f (Matrix 3 3 (Vector a))
getSigExtMat g =
  BSDP.getIsolatedMat (blockParams g) (derivsVecGNYPsiEps g) (runTagged2 (nGroup g, externalDims g) (mat (ExternalSigChannel $ Fixed deltaSig)))
  where
    deltaSig = case objective g of
      FeasibilitySig delta _ -> delta
      _ -> error "can't call getSigEpsMat without objective FeasibilitySig"

bulkConstraints :: GNYPsiEps -> [BulkConstraint]
bulkConstraints f = do
  parity <- [minBound .. maxBound]
  oNRep <- [minBound .. maxBound]
  l <- case (parity, oNRep) of
    (_, ONVector)                    -> filter (not . HI.isInteger) (spins f)
    (B3d.ParityEven,ONSinglet)       -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSinglet)       -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONSymTensor)     -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSymTensor)     -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONAntiSymTensor) -> filter HI.isOddInteger (spins f)
    (B3d.ParityOdd, ONAntiSymTensor) -> filter (\x -> (x/=0) && (HI.isInteger x)) (spins f)
  (delta, range) <- listDeltas (ChannelType l parity oNRep) (spectrum f)
  let iConformalRep = B3d.ConformalRep delta l
  pure $ case (parity, HI.isEvenInteger l, l, oNRep) of
    (B3d.ParityEven, True,  0, ONSinglet)       -> BulkConstraint range $ Scalar_ParityEven_Singlet delta
    (B3d.ParityEven, True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityEven_Singlet iConformalRep
    (B3d.ParityOdd,  True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityOdd_Singlet iConformalRep
    (B3d.ParityOdd,  False, _, ONSinglet)       -> BulkConstraint range $ SpinOdd_ParityOdd_Singlet iConformalRep
    (B3d.ParityEven, True,  0, ONSymTensor)     -> BulkConstraint range $ Scalar_ParityEven_TensorSym delta
    (B3d.ParityEven, True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityEven_TensorSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityOdd_TensorSym iConformalRep
    (B3d.ParityOdd,  False, _, ONSymTensor)     -> BulkConstraint range $ SpinOdd_ParityOdd_TensorSym iConformalRep
    (B3d.ParityEven, False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityEven_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONAntiSymTensor) -> BulkConstraint range $ SpinEven_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityEven, False, _, ONVector)        -> BulkConstraint range $ ParityEven_Vector iConformalRep
    (B3d.ParityOdd,  False, _, ONVector)        -> BulkConstraint range $ ParityOdd_Vector iConformalRep
    _ -> error $ "Internal representation disallowed: " ++ show((parity,l,oNRep))

gnyepsSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext B3d.Block3d a m
     )
  => GNYPsiEps
  -> SDPB.SDP m a
gnyepsSDP f@GNYPsiEps{..} = runTagged2 (nGroup,externalDims) $ do
  let dv = derivsVecGNYPsiEps f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  extMat <- mat ExternalOpChannel
  unit   <- mat IdentityChannel
  stress <- mat StressTensorChannel
  current <- mat CurrentChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv stress --Isolated stress
    extCons mLambda = case mLambda of
      Nothing     -> BSDP.isolatedConstraint blockParams dv extMat
      Just lambda -> BSDP.isolatedConstraint blockParams dv $
                     L.bilinearPair (fmap fromRational lambda) extMat
    currentCons = BSDP.isolatedConstraint blockParams dv current
  (cons, obj, norm) <- case objective of
    Feasibility mLambda -> pure $
      ( bulk ++ [extCons mLambda, stressCons, currentCons]
      , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
      , BSDP.bootstrapNormalization blockParams dv $ unit
      )
    StressTensorOPEBound dir -> pure $
      ( bulk ++ [extCons Nothing, currentCons]
      , BSDP.bootstrapObjective blockParams dv $ unit
      , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
      )
    FeasibilitySig deltaSig mLambda -> do
      sigExtMat <- mat (ExternalSigChannel $ Fixed deltaSig)
      let sigExtCons = case mLambda of
            Just lambda -> BSDP.isolatedConstraint blockParams dv $
                           L.bilinearPair (fmap fromRational lambda) sigExtMat
            Nothing     -> BSDP.isolatedConstraint blockParams dv sigExtMat
      pure $
        ( bulk ++ [sigExtCons, stressCons, currentCons]
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
      -- ConservedCurrentOPEBound dir ->
      --   ( bulk ++ [extCons, stressCons]
      --   , BSDP.bootstrapObjective blockParams dv $ unit
      --   , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ current
      --   )
      --ExternalOPEBound dir ->
      --  ( bulk ++ [stressCons, currentCons]
      --  , BSDP.bootstrapObjective blockParams dv $ unit
      --  , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ extFS
      --  )
  return $ SDPB.SDP
    { SDPB.objective     = obj
    , SDPB.normalization = norm
    , SDPB.positiveConstraints      = cons
    }

instance ToSDP GNYPsiEps where
  type SDPFetchKeys GNYPsiEps = '[ B3d.BlockTableKey ]
  toSDP = gnyepsSDP

instance SDPFetchBuildConfig GNYPsiEps where
  sdpFetchConfig _ _ boundFiles =
    liftIO . B3d.readBlockTable (blockDir boundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig boundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig boundFiles

instance Static (Binary GNYPsiEps)              where closureDict = cPtr (static Dict)
instance Static (Show GNYPsiEps)                where closureDict = cPtr (static Dict)
instance Static (ToSDP GNYPsiEps)               where closureDict = cPtr (static Dict)
instance Static (ToJSON GNYPsiEps)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig GNYPsiEps) where closureDict = cPtr (static Dict)
instance Static (BuildInJob GNYPsiEps)          where closureDict = cPtr (static Dict)
