{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.Fermions3d.GNYPsiEpsTest2020 where

import qualified Blocks.Blocks3d                      as B3d
import qualified Blocks.Delta                         as Delta
import qualified Bootstrap.Bounds.Spectrum            as Spectrum
import           Bootstrap.Math.Linear                (Matrix, V, fromV, toM,
                                                       toV)
import           Bounds.Fermions3d.GNY                (ChannelType (..))
import qualified Bounds.Fermions3d.GNYPsiEps          as GNYPsiEps
import           Bounds.Scalars3d.ONRep               (ONRep (..))
import           Control.Monad                        (void)
import           Control.Monad.Reader                 (local)
import qualified Data.Map.Strict                      as Map
import qualified Data.Set                             as Set
import           Data.Text                            (Text)
import           Hyperion
import           Hyperion.Bootstrap.Bound             (Bound (..))
import qualified Hyperion.Bootstrap.Bound             as Bound
import           Hyperion.Bootstrap.DelaunaySearch    (delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main              (unknownProgram)
import           Hyperion.Bootstrap.OPESearch         (BilinearForms (..),
                                                       OPESearchConfig (..),
                                                       TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch         as OPE
import qualified Hyperion.Bootstrap.Params            as Params
import qualified Hyperion.Database                    as DB
import           Hyperion.Util                        (hour)
import           Projects.Fermions3d.Defaults         (defaultBoundConfig,
                                                       defaultDelaunayConfig,
                                                       defaultQuadraticNetConfig)
import qualified Projects.Fermions3d.GNYData2020Chi35 as GNYData
import           SDPB                                 (Params (..))
import qualified SDPB

remoteGNYPsiEpsOPESearch
  :: TrackedMap Cluster (Bound Int GNYPsiEps.GNYPsiEps) FilePath
  -> TrackedMap Cluster (Bound Int GNYPsiEps.GNYPsiEps) (V 2 Rational)
  -> Rational
  -> Rational
  -> Bound Int GNYPsiEps.GNYPsiEps
  -> Cluster Bool
remoteGNYPsiEpsOPESearch checkpointMap lambdaMap thetaMin thetaMax =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    initialBilinearForms =
      BilinearForms 1e-16 [(Nothing, OPE.thetaIntervalFormApprox 1e-16 thetaMin thetaMax)]
    opeSearchCfg =
      OPESearchConfig setLambda GNYPsiEps.getExtMat OPE.queryAllowedBoolFunction
    setLambda :: (V 2 Rational) -> Bound prec GNYPsiEps.GNYPsiEps -> Bound prec GNYPsiEps.GNYPsiEps
    setLambda lambda bound = bound
        { boundKey = (boundKey bound)
          { GNYPsiEps.objective = GNYPsiEps.Feasibility $ Just lambda}
        }

remoteGNYSigExtOPESearch
  :: TrackedMap Cluster (Bound Int GNYPsiEps.GNYPsiEps) FilePath
  -> TrackedMap Cluster (Bound Int GNYPsiEps.GNYPsiEps) (V 3 Rational)
  -> BilinearForms 3
  -> Bound Int GNYPsiEps.GNYPsiEps
  -> Cluster Bool
remoteGNYSigExtOPESearch checkpointMap lambdaMap initialBilinearForms =
  OPE.remoteOPESearch (cPtr (static opeSearchCfg)) checkpointMap lambdaMap initialBilinearForms
  where
    opeSearchCfg =
      OPESearchConfig setLambda GNYPsiEps.getSigExtMat $ OPE.queryAllowedMixed qmConfig
    setLambda :: (V 3 Rational) -> Bound prec GNYPsiEps.GNYPsiEps -> Bound prec GNYPsiEps.GNYPsiEps
    setLambda lambda bound = case (GNYPsiEps.objective $ boundKey bound) of
      GNYPsiEps.FeasibilitySig deltaSig _ ->
        bound
        { boundKey = (boundKey bound)
          { GNYPsiEps.objective = GNYPsiEps.FeasibilitySig deltaSig $ Just lambda}
        }
      _ -> error "can't do OPE search without FeasibilitySig objective"
    qmConfig = OPE.QueryMixedConfig
      { qmQuadraticNetConfig = defaultQuadraticNetConfig
      , qmDescentConfig      = OPE.defaultDescentConfig
      , qmResolution         = 1e-16
      , qmPrecision          = 256
      , qmHessianLineSteps   = 200
      , qmHessianLineAverage = 10
      }


-- | Convert a 'Bound prec GNY' to a 2-dimensional vector given by
--
-- (deltaPsi, deltaSig)
--
-- It is important that the ordering convention for 2-dimensional
-- vectors of dimensions be kept consistent in all computations.
-- deltaExtToV :: Bound prec GNYPsiEps.GNYPsiEps -> V 3 Rational
-- deltaExtToV Bound { bound = GNYPsiEps.GNYPsiEps { externalDims = eds } } =
--   toV (GNYPsiEps.deltaPsi eds, GNYPsiEps.deltaSig eds, GNYPsiEps.deltaEps eds)

deltaExtEpsToV :: Bound prec GNYPsiEps.GNYPsiEps -> V 3 Rational
deltaExtEpsToV Bound { boundKey = GNYPsiEps.GNYPsiEps { externalDims = eds, spectrum = s } } =
  toV (GNYPsiEps.deltaPsi eds, deltaSig, GNYPsiEps.deltaEps eds)
  where
    deltaSig = case Set.toAscList (Spectrum.deltaIsolated s sigChannel) of
      Delta.Fixed d : _ -> d
      _ -> error $ "Spectrum does not contain an isolated operator in the sigma channel: " ++ show s
    sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet

gnyPsiEpsFeasibleDefaultGaps :: V 3 Rational -> Maybe (V 2 Rational) -> Rational -> Int -> GNYPsiEps.GNYPsiEps
gnyPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYPsiEps.GNYPsiEps
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = Spectrum.setGaps
                   [ (psiChannel, 2)
                   , (chiChannel, 3.5)
                   , (sigChannel, max 3 deltaSig)
                   , (epsChannel, 3)
                   , (sigTChannel, 2)
                     -- , (epsTChannel, 3)
                   ] $ Spectrum.addIsolated sigChannel deltaSig Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYPsiEps.Feasibility mLambda
  , externalDims = GNYPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = ChannelType (1/2) B3d.ParityEven ONVector
    sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet
    epsChannel  = ChannelType 0 B3d.ParityEven ONSinglet
    chiChannel  = ChannelType (1/2) B3d.ParityOdd ONVector
    sigTChannel = ChannelType 0 B3d.ParityOdd ONSymTensor
    --epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
    (deltaPsi,deltaSig, deltaEps) = fromV deltaExts

gnyExtSigFeasibleDefaultGaps :: V 3 Rational -> Maybe (V 3 Rational) -> Rational -> Int -> GNYPsiEps.GNYPsiEps
gnyExtSigFeasibleDefaultGaps deltaExts mLambda ngroup nmax = GNYPsiEps.GNYPsiEps
  { -- (ChannelType l B3d.Parity ONRep, Rational gap)
    spectrum     = Spectrum.setGaps
                   [ (psiChannel, 2)
                   , (chiChannel, 3.5)
                   , (sigChannel, max 3 deltaSig)
                   , (epsChannel, 3)
                   , (sigTChannel, 2)
                     -- , (epsTChannel, 3)
                   ] Spectrum.unitarySpectrum
  , nGroup       = ngroup
  , objective    = GNYPsiEps.FeasibilitySig deltaSig mLambda
  , externalDims = GNYPsiEps.ExternalDims {..}
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
  }
  where
    psiChannel  = ChannelType (1/2) B3d.ParityEven ONVector
    sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet
    epsChannel  = ChannelType 0 B3d.ParityEven ONSinglet
    chiChannel  = ChannelType (1/2) B3d.ParityOdd ONVector
    sigTChannel = ChannelType 0 B3d.ParityOdd ONSymTensor
    --epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
    (deltaPsi,deltaSig, deltaEps) = fromV deltaExts

-- gny2dFeasibleDefaultGaps :: V 2 Rational -> Rational -> Int -> GNYPsiEps.GNYPsiEps
-- gny2dFeasibleDefaultGaps deltaExts ngroup nmax = GNYPsiEps.GNYPsiEps
--   { -- (ChannelType l B3d.Parity ONRep, Rational gap)
--     spectrum     = setTwistGap 1e-6 $ Spectrum.setGaps [ (psiChannel, 2)
--                            , (chiChannel, 3.5)
--                            , (sigChannel, 3)
--                            , (epsChannel, 3)
--                            , (sigTChannel, 2)
--                            -- , (epsTChannel, 3)
--                            ]  Spectrum.unitarySpectrum
--   , nGroup       = ngroup
--   , objective    = GNYPsiEps.Feasibility Nothing
--   , externalDims = GNYPsiEps.ExternalDims {..}
--   , blockParams  = Params.block3dParamsNmax nmax
--   , spins        = Params.gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
--   }
--   where
--     psiChannel  = ChannelType (1/2) B3d.ParityEven ONVector
--     sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet
--     epsChannel  = ChannelType 0 B3d.ParityEven ONSinglet
--     chiChannel  = ChannelType (1/2) B3d.ParityOdd ONVector
--     sigTChannel = ChannelType 0 B3d.ParityOdd ONSymTensor
--     epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
--     (deltaPsi,deltaSig) = fromV deltaExts
--     deltaEps = 1.65
--
--
-- gny4dFeasibleDefaultGaps :: V 4 Rational -> Rational -> Int -> GNYPsiEps.GNYPsiEps
-- gny4dFeasibleDefaultGaps deltaExts ngroup nmax = GNYPsiEps.GNYPsiEps
--   { -- (ChannelType l B3d.Parity ONRep, Rational gap)
--     spectrum     = Spectrum.setGaps [ (psiChannel, 2)
--                            , (chiChannel, 3.5)
--                            , (sigChannel, deltaSigP)
--                            , (epsChannel, 3)
--                            , (sigTChannel, 2)
--                            -- , (epsTChannel, 3)
--                            ]  Spectrum.unitarySpectrum
--   , nGroup       = ngroup
--   , objective    = GNYPsiEps.Feasibility Nothing
--   , externalDims = GNYPsiEps.ExternalDims {..}
--   , blockParams  = Params.block3dParamsNmax nmax
--   , spins        = Params.gnyspinsNmax nmax -- gynspinsNmax output [Half-int], defined in ParamCollection.hs
--   }
--   where
--     psiChannel  = ChannelType (1/2) B3d.ParityEven ONVector
--     sigChannel  = ChannelType 0 B3d.ParityOdd ONSinglet
--     epsChannel  = ChannelType 0 B3d.ParityEven ONSinglet
--     chiChannel  = ChannelType (1/2) B3d.ParityOdd ONVector
--     sigTChannel = ChannelType 0 B3d.ParityOdd ONSymTensor
--     epsTChannel = ChannelType 0 B3d.ParityEven ONSymTensor
--     (deltaPsi,deltaSig, deltaEps, deltaSigP) = fromV deltaExts




delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"


boundsProgram :: Text -> Cluster ()

-------------------
-- N=2 3d search --
-------------------

boundsProgram "GNYPsiEps_test_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV GNYData.epsN2nmax6DualPoints--n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    bound deltaExts = Bound
      { boundKey = gnyPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }

-- boundsProgram "GNY2d_test_ngroup2_nmax6" =
--   local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ void $ do
--   mapConcurrently_ (Bound.remoteCompute . bound) $
--      fmap toV [(1.068, 0.65), (1.069, 0.66), (1.071, 0.645)]
--   where
--     nmax = 6
--     ngroup = 2
--     bound deltaExts = Bound
--       { boundKey = gny2dFeasibleDefaultGaps deltaExts ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (Params.jumpFindingParams nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--
--
--
-- boundsProgram "GNY4d_test_ngroup2_nmax6" =
--   local (setJobType (MPIJob 1 12) . setJobTime (8*hour)) $ void $ do
--   mapConcurrently_ (Bound.remoteCompute . bound) $
--      fmap toV [(1.06 + x/250, 0.64 + y/50, 1.3 + z/10, 2.6 + s/10) | x <-[0..3], y <- [0..4], z <- [0..5], s <- [0..4]]
--   where
--     nmax = 6
--     ngroup = 2
--     bound deltaExts = Bound
--       { boundKey = gny4dFeasibleDefaultGaps deltaExts ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (Params.jumpFindingParams nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--
-- boundsProgram "GNYSigPsiEp_2d_island_ngroup2_nmax6" =
--  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 6
--     ngroup = 2
--     affine = GNYData.gny2AffineNmax6
--     f :: V 2 Rational -> Cluster Bool
--     f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
--     bound deltaExts = Bound
--       { boundKey = gny2dFeasibleDefaultGaps deltaExts ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (Params.jumpFindingParams nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 10 100
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] -- ++ [(p, Just False) | p <- initialDisallowed]
--       where
--         --initialDisallowed = fmap toV $ [()]--GNYData.n2nmax6DualPoints
--         initialAllowed = fmap toV $ [(1.068, 0.65), (1.069, 0.66), (1.071, 0.645)]




boundsProgram "GNYPsiEp_island_ngroup2_nmax6" =
 local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    f :: V 3 Rational -> Cluster Bool
    f deltaExts = fmap SDPB.isPrimalFeasible (Bound.remoteCompute $ bound deltaExts)
    bound deltaExts = Bound
      { boundKey = gnyPsiEpsFeasibleDefaultGaps deltaExts Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] -- ++ [(p, Just False) | p <- initialDisallowed]
      where
        --initialDisallowed = fmap toV $ [()]--GNYData.n2nmax6DualPoints
        initialAllowed = fmap toV $ [(1.06, 0.6625,1.1), (2867 / 2700, 0.6375, 1.2), (2867 / 2700,0.65,1.4),(718 /675, 0.625, 1.8)]

boundsProgram "GNYPsiEp_island_ngroup2_nmax6_with_checkpoint_map" =
  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
  local (setJobType (MPIJob 1 12) . setJobMemory "36G" . setJobTime (8*hour)) $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
  let
    f :: V 3 Rational -> Cluster Bool
    f deltaV = fmap SDPB.isPrimalFeasible $
      Bound.remoteComputeWithCheckpointMap checkpointMap $
      bound deltaV
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dWindow
    bound deltaV = Bound
      { boundKey = gnyPsiEpsFeasibleDefaultGaps deltaV Nothing ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768
                                             , findPrimalFeasible = False }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 5 200
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] -- ++ [(p, Just False) | p <- initialDisallowed]
      where
        --initialDisallowed = fmap toV $ GNYData.epsN2nmax6DualPoints
        initialAllowed = fmap toV $ GNYData.epsN2nmax6PrimalPoints
    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
    -- here. It would be used as the default return value for the
    -- checkpointMap, until at least one completed checkpoint gets
    -- recorded.
    mInitialCheckpoint = Nothing

--------------
-- N=2 OPE ---
--------------

boundsProgram "GNYPsiEps_OPEtest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) [-1.5,-1.4..1.5] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    mLambda theta = Just $ OPE.thetaVectorApprox 1e-16 theta
    bound theta = Bound
      { boundKey = gnyPsiEpsFeasibleDefaultGaps (toV (1.069,0.65,1.66)) (mLambda theta) ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYPsiEps_OPESearchTest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtEpsToV mLambda
  mapConcurrently_ (remoteGNYPsiEpsOPESearch checkpointMap lambdaMap (-1.57) 1.57 . bound) $
     fmap toV [(1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    mLambda = Just $ OPE.thetaVectorApprox 1e-16 1.5
    bound deltaExts = Bound
      { boundKey = gnyPsiEpsFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYPsiEps_ExtSig_OPEtest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  mapConcurrently_ (Bound.remoteCompute . bound) $
     fmap toV [(1.069,0.65,1.66)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    mLambda = Just $ toV (1,0.238476, 0.971148)
    bound deltaExts = Bound
      { boundKey = gnyExtSigFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768}
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYPsiEps_ExtSig_OPESearchTest_ngroup2_nmax6" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtEpsToV Nothing -- $ Just $ toV (0.5,0.25,1.65)
  mapConcurrently_ (remoteGNYSigExtOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.0686, 0.651, 1.70)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 6
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    bilinearMat :: Matrix 3 3 Rational
    bilinearMat = toM
      ( (-1,0,0)
      , (0,-1,0)
      , (0,0,0.01)
      )
    bilinearMat2 = toM
      ( (1,0,0)
      , (0,1,0)
      , (0,0,-100)
      )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat),(Nothing,bilinearMat2)]
    mLambda = Just $ toV (1,1,1)
    bound deltaExts = Bound
      { boundKey = gnyExtSigFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "GNYPsiEps_ExtSig_OPESearchTest_ngroup2_nmax10" =
  local (setJobType (MPIJob 1 36) . setJobTime (8*hour) . setJobMemory "100G" . setSlurmPartition "pi_poland,day,scavenge") $ void $ do
  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV Nothing
  lambdaMap     <- Bound.newLambdaMap affine deltaExtEpsToV $ Just $ toV (-0.52,0.24,1.65)
  mapConcurrently_ (remoteGNYSigExtOPESearch checkpointMap lambdaMap initialBilinearForms . bound) $
     fmap toV [(1.069,0.65,1.66)] --n2nmax6PrimalPoints --GNYData.epsN2nmax6DualPoints --[(1.06 + x/200, 0.6 + y/50, 1.3 + z/10) | x <-[0..4], y <- [0..5], z <- [0..7]]
  where
    nmax = 10
    ngroup = 2
    affine = GNYData.gny2Affine3dNmax6
    bilinearMat :: Matrix 3 3 Rational
    bilinearMat = toM
      ( (1,0,0)
      , (0,1,0)
      , (0,0,-1)
      )
    initialBilinearForms = BilinearForms 1e-32 [(Nothing, bilinearMat)]
    mLambda = Just $ toV (0,0,0)
    bound deltaExts = Bound
      { boundKey = gnyExtSigFeasibleDefaultGaps deltaExts mLambda ngroup nmax
      , precision = B3d.precision (Params.block3dParamsNmax nmax)
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

-- boundsProgram "GNYPsiEps_OPEDelaunay_ngroup2_nmax6" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 9) . setJobMemory "36G" . setJobTime (8*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       Bound.remoteComputeWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 6
--     ngroup = 2
--     affine = GNYData.gny2Affine3dWindow
--     bound deltaV = Bound
--       { boundKey = gnyPsiEpsFeasibleDefaultGaps deltaV Nothing ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (Params.jumpFindingParams nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 5 200
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] -- ++ [(p, Just False) | p <- initialDisallowed]
--       where
--         --initialDisallowed = fmap toV $ GNYData.epsN2nmax6DualPoints
--         initialAllowed = fmap toV $ GNYData.epsN2nmax6PrimalPoints
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing

-- boundsProgram "GNY_3d_island_ngroup2_nmax8_with_checkpoint_map" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 8
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax8
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 1 2
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax8DualPoints
--         initialAllowed = fmap toV $ GNYData.n2nmax8PrimalPoints
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing
--
-- boundsProgram "GNY_3d_island_ngroup2_nmax10_with_checkpoint_map" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 24) . setJobMemory "72G" . setJobTime (8*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 10
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax10
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 10 100
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax10DualPoints2
--         initialAllowed = fmap toV $ GNYData.n2nmax10PrimalPoints2
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing  --"/central/groups/dssimmon/aike/test/data/2020-11/KJrLH/Object_uwnS1eRBtHdv-MEYw0V9aB0Jy_TTwoj-5qmLNVakeFg/ck"
--
-- boundsProgram "GNY_3d_island_ngroup2_nmax12_with_checkpoint_map" =
--   -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--   local (setJobType (MPIJob 1 32) . setJobMemory "96G" . setJobTime (24*hour)) $ do
--   checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--   let
--     f :: V 3 Rational -> Cluster Bool
--     f deltaV = fmap SDPB.isPrimalFeasible $
--       remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--       bound deltaV
--   void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--   where
--     nmax = 12
--     ngroup = 2
--     affine = GNYData.gny2Affine3dNmax12
--     bound deltaV = Bound
--       { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--       , precision = B3d.precision (Params.block3dParamsNmax nmax)
--       , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                              , findPrimalFeasible = False }
--       , boundConfig = defaultBoundConfig
--       }
--     delaunayConfig = defaultDelaunayConfig 20 300
--     initialPts = Map.fromList $
--       [(p, Just True)  | p <- initialAllowed] ++
--       [(p, Just False) | p <- initialDisallowed]
--       where
--         initialDisallowed = fmap toV $ GNYData.n2nmax12DualPoints
--         initialAllowed = fmap toV $ GNYData.n2nmax12PrimalPoints
--     -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--     -- here. It would be used as the default return value for the
--     -- checkpointMap, until at least one completed checkpoint gets
--     -- recorded.
--     mInitialCheckpoint = Nothing




----------------
-- N=4 search --
----------------


--boundsProgram "GNY_3d_island_ngroup4_nmax6" =
--  local (setJobType (MPIJob 1 7) . setJobMemory "20G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaV)
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 2000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV []
--        initialAllowed = fmap toV [(1.044,0.77,1.8)]
--
--boundsProgram "GNY_3d_island_ngroup4_nmax6_with_checkpoint_map" =
--  -- To run at Yale, probably best to set 8 CPUs -> 7 CPUs and 24GB -> 20GB
--  local (setJobType (MPIJob 1 8) . setJobMemory "24G" . setJobTime (1*hour)) $ do
--  checkpointMap <- Bound.newCheckpointMap affine deltaExtEpsToV mInitialCheckpoint
--  let
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible $
--      remoteComputeGNYBoundWithCheckpointMap checkpointMap $
--      bound deltaV
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 2000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV []
--        initialAllowed = fmap toV [(1.044,0.77,1.8)]
--    -- Instead of 'Nothing', we could supply a pre-computed checkpoint
--    -- here. It would be used as the default return value for the
--    -- checkpointMap, until at least one completed checkpoint gets
--    -- recorded.
--    mInitialCheckpoint = Nothing
--
--boundsProgram "GNY_3d_island_ngroup4_nmax8" =
--  local (setJobType (MPIJob 1 18) . setJobMemory "40G" . setJobTime (4*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 8
--    ngroup = 4
--    affine = gny4Affine3dNmax6
--    f :: V 3 Rational -> Cluster Bool
--    f deltaV = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaV)
--    bound deltaV = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaV ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 26 1000
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialAllowed = fmap toV [(9341401777594260840536377/8947306799824896000000000,214138321200063753560017463/286313817594396672000000000,2367555402948089231516369081/1431569087971983360000000000)]
--
--boundsProgram "GNY_Island_ngroup4_nmax8" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (2*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 8
--    ngroup = 4
--    affine = gny4AffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYBound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = gnyFeasibleDefaultGaps deltaExts ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 200
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = fmap toV $
--          [(409/400, 3/4), (419/400, 5/8), (419/400, 7/8), (1717/1600, 21/32), (26/25, 23/32), (26/25, 25/32),
--          (417/400, 3/4), (13417/12800, 175/256), (13907/12800, 159/256), (27019/25600, 445/512),
--          (13421/12800, 213/256), (53773/51200, 691/1024), (667/640, 189/256), (13591/12800, 245/256),
--          (107763/102400, 1749/2048), (667/640, 195/256), (444069/409600, 5265/8192), (439919/409600, 5423/8192),
--          (213561/204800, 3011/4096), (53749/51200, 53/64), (53583/51200, 829/1024), (106737/102400, 1485/2048),
--          (887183/819200, 10707/16384), (1779077/1638400, 21233/32768), (106761/102400, 6211/8192),
--          (1766093/1638400, 21529/32768), (108317/102400, 339/512), (112077/102400, 1245/2048),
--          (225041/204800, 2463/4096), (108349/102400, 1859/2048), (214833/204800, 841/1024),
--          (85403/81920, 5871/8192), (1429953/1310720, 84529/131072), (34423/32768, 13677/16384),
--          (213807/204800, 2893/4096), (3449163/3276800, 55701/65536), (871923/819200, 32283/32768),
--          (6896029/6553600, 86631/131072), (2844053/2621440, 171461/262144), (697303/655360, 124831/131072),
--          (1737549/1638400, 59879/65536), (173461/163840, 14663/16384), (7078993/6553600, 86413/131072),
--          (28685061/26214400, 335985/524288), (853501/819200, 12811/16384), (3507567/3276800, 43437/65536),
--          (28505817/26214400, 343541/524288), (3603713/3276800, 39327/65536), (106883/102400, 811/1024),
--          (13791067/13107200, 221701/262144), (1380123/1310720, 87621/131072),
--          (115135429/104857600, 1333169/2097152), (27664593/26214400, 349571/524288),
--          (13887039/13107200, 174605/262144), (6964789/6553600, 245791/262144),
--          (114733593/104857600, 1361205/2097152), (341707/327680, 25633/32768),
--          (18441389/16777216, 5319313/8388608), (230230021/209715200, 2702193/4194304),
--          (2777197/2621440, 473535/524288), (2741027/2621440, 178921/262144), (5581587/5242880, 1013611/1048576),
--          (866271/819200, 10919/16384), (27568087/26214400, 354357/524288),
--          (919292983/838860800, 10921435/16777216), (1843816687/1677721600, 21634419/33554432),
--          (457561549/419430400, 5471257/8388608), (115312691/104857600, 1317549/2097152),
--          (13808413/13107200, 224403/262144), (11532699/10485760, 635737/1048576), (174789/163840, 32637/32768),
--          (227740279/209715200, 2738203/4194304), (13711891/13107200, 180613/262144),
--          (3684856907/3355443200, 43551647/67108864), (110440707/104857600, 1410681/2097152),
--          (55861347/52428800, 2047887/2097152), (27846429/26214400, 974439/1048576),
--          (6969181/6553600, 87225/131072), (230619257/209715200, 2694183/4194304),
--          (223354153/209715200, 8073533/8388608), (92257093/83886080, 5367879/8388608),
--          (563657/524288, 173165/262144), (1831714673/1677721600, 21859629/33554432),
--          (27412063/26214400, 363113/524288), (28244177/26214400, 346109/524288),
--         (110656503/104857600, 1862041/2097152)]
--        initialAllowed = [toV (1.044,0.77)]
--
--boundsProgram "GNY_test_ngroup4_nmax6" =
--  local (setJobType (MPIJob 1 36) . setJobTime (1*hour)) $ void $ do
--  mapConcurrently_ (remoteComputeGNYBound . bound) $
--     fmap toV [(1.044,0.77,1+x/10) | x <-[0..10]]
--  where
--    nmax = 6
--    ngroup = 4
--    bound deltaExts = Bound
--      { boundKey = gny3dSearchDefaultGaps deltaExts ngroup nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--
--
--
--boundsProgram "SuperIsing_Island_nmax6" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    affine = superIsingAffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYN1Bound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = superIsingFeasibleDefaultGaps deltaExts nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 200
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = []
--        initialAllowed = [toV (1.085,0.6)]
--
--boundsProgram "BigJumpSearch_nmax6" =
--  local (setJobType (MPIJob 1 9) . setJobMemory "50G" . setJobTime (1*hour)) $ do
--  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts f
--  where
--    nmax = 6
--    affine = superIsingAffineNmax6
--    f :: V 2 Rational -> Cluster Bool
--    f deltaExts = fmap SDPB.isPrimalFeasible (remoteComputeGNYN1Bound $ bound deltaExts)
--    bound deltaExts = Bound
--      { boundKey = superIsingFeasibleDefaultGaps deltaExts nmax
--      , precision = B3d.precision (Params.block3dParamsNmax nmax)
--      , solverParams = (sdpbParamsNmax nmax) { precision = 768
--                                             , findPrimalFeasible = False }
--      , boundConfig = defaultBoundConfig
--      }
--    delaunayConfig = defaultDelaunayConfig 20 400
--    initialPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
--        initialDisallowed = []
--        initialAllowed = [toV (1.085,0.6)]



boundsProgram p = unknownProgram p



--gny4IslandPts :: Map.Map (V 3 Rational) (Maybe Bool)
--gny4IslandPts = Map.fromList $
--      [(p, Just True)  | p <- initialAllowed] ++
--      [(p, Just False) | p <- initialDisallowed]
--      where
