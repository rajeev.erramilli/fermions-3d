#!/bin/bash

module load cmake/3.10.2 gcc/7.3.0 openmpi/3.0.0 boost/1_68_0-gcc730 mathematica/11.1
export OPENBLAS_NUM_THREADS=1

echo srun /central/groups/dssimmon/dsd/sdpdd_experimental/install/bin/sdpd $@
srun /central/groups/dssimmon/dsd/sdpdd_experimental/install/bin/sdpd $@

