#!/bin/zsh

source /etc/profile.d/modules.sh

where module

module purge
module load gnu/7.2.0 openmpi_ib/3.1.4 cmake eigen mpfr

echo /home/wlandry/runs/ibrun -v /home/wlandry/gnu/outer/install/bin/sdpb $@
/home/wlandry/runs/ibrun -v /home/wlandry/gnu/outer/install/bin/sdpb $@
